package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);
    public static DotsAndBoxesGrid grid;

    @BeforeAll
    public static void beforeAll(){
        logger.info("Running 'before all' setup");
    }

    @BeforeEach
    public void beforeEach(){
        logger.info("Running 'before each' setup");
        grid = new DotsAndBoxesGrid(3,3,2);
    }

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.

    @Test
    public void testBoxIsComplete() {
        logger.info("Checking completed box is correctly detected");

        //empty grid, test top left box
        assertFalse(grid.boxComplete(0,0));

        //draw top horizontal and test
        grid.drawHorizontal(0,0,1);
        assertFalse(grid.boxComplete(0,0));

        //draw left vertical and test
        grid.drawVertical(0,0,1);
        assertFalse(grid.boxComplete(0,0));

        //draw bottom horizontal and test
        grid.drawHorizontal(0,1,1);
        assertFalse(grid.boxComplete(0,0));

        //draw right vertical and test, box now complete
        grid.drawVertical(1,0,1);
        assertTrue(grid.boxComplete(0,0));
    }

    @Test
    public void testCantRedrawLine(){
        //draw line first time
        grid.drawHorizontal(0,0,1);
        assertThrows(IllegalStateException.class, () -> grid.drawHorizontal(0,0,2));
    }
}
